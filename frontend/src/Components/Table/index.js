import React, {Component} from 'react';
import { rowStyle } from '../../helpers';
import './Table.scss'

import MetricsBar from '../MetricsBar'

const column_types = {
    'mttl': [
        {field: 'ksat_id', title: 'ID', sortable: true},
        {field: '_id', title: 'FULL_ID', sortable: true, visible: false},
        {field: 'description', title: 'Description', class: "header_description", sortable: true},
        {field: 'requirement_src', class:'click_filter', title: 'Source', sortable: true},
        {field: 'requirement_owner', class:'click_filter', title: 'Owner', sortable: true},
        {field: 'parent', title: 'Parent(s)', class: "children_description", sortable: true},
        {field: 'children', title: 'Children', class: "children_description", sortable: true},
        {field: 'work-roles/specializations', class: "click_filter", title: 'Work Roles/Specializations', sortable: true},
        {field: 'topic', class: 'click_filter', title: 'Topic', sortable: true},
        {field: 'proficiency', title: 'Proficiency', sortable: true},
        {field: 'training_links', title: 'Training Covered', class: "extra_header_description", sortable: true},
        {field: 'eval_links', title: 'Eval Covered', class: "extra_header_description", sortable: true},
        {field: 'comments', title: 'Comments', class: "extra_header_description", sortable: true, visible: false}
    ],
    'wrspec': [
        {field: 'wr_spec', title: 'Work Role/Specialization', sortable: true},
        {field: 'Tasks', title: 'Tasks', class: "min_header_description"},
        {field: 'Abilities', title: 'Abilities', class: "header_description"},
        {field: 'Skills', title: 'Skills', class: "min_header_description"},
        {field: 'Knowledge', title: 'Knowledge', class: "extra_header_description"},
    ],
    'nowrspec': [
        {field: 'Tasks', title: 'Tasks', class: "min_header_description"},
        {field: 'Abilities', title: 'Abilities', class: "header_description"},
        {field: 'Skills', title: 'Skills', class: "min_header_description"},
        {field: 'Knowledge', title: 'Knowledge', class: "extra_header_description"},
    ]
};

var tableName;

class Table extends Component {

    clearSearch(){
        $(tableName).bootstrapTable('resetSearch');
    }
    
    componentDidMount() {

        for(var i = 0; i < this.data.length; i++){
            if(this.data[i]['requirement_src']?.substr(0,6) !== '<span>' ||  this.data[i]['requirement_owner']?.substr(0,6) !== '<span>' ||  this.data[i]['work-roles/specializations']?.substr(0,6) !== '<span>' ||  this.data[i]['topic']?.substr(0,6) !== '<span>'){
                this.data[i]['requirement_src'] = '<span>'+this.data[i]['requirement_src']+'</span>';
                this.data[i]['requirement_owner'] = '<span>'+this.data[i]['requirement_owner']+'</span>';
                this.data[i]['work-roles/specializations'] = '<span>'+this.data[i]['work-roles/specializations']+'</span>';
                this.data[i]['topic'] = '<span>'+this.data[i]['topic']+'</span>';
                if(this.data[i]['eval_links'].length > 0){this.data[i]['eval_links'] = '<div class="max-height">'+this.data[i]['eval_links']+'</div>';}
                this.data[i]['children'] = '<div class="max-height">'+this.data[i]['children']+'</div>';
                this.data[i]['description'] = '<div class="max-height">'+this.data[i]['description']+'</div>';
                this.data[i]['ksat_id'] = '<i class="fa fa-plus"></i><i class="fa fa-minus"></i>'+this.data[i]['ksat_id'];

            } 
        }

        this.defaultSettings = {
            data: this.data,
            height: 1,
            search: true,
            pagination: true,
            pageSize: 100,
            pageList: [25, 50, 100, 1000],
            showFullscreen: true,
            showColumns: true,
            showExport: true,
            exportDataType: 'all',
            exportTypes: ['csv', 'excel'],
            exportOptions: {
                    ignoreColumn: [],
                    fileName: function() {
                        return this.tableId + '-export'
                    }
            },
            columns: this.columns
        };

        if (this.props.rowStyle) {
            this.defaultSettings.rowStyle = rowStyle;
        }
         
        /*global $*/
        $('.' + this.searchName).bootstrapTable(this.defaultSettings)
        tableName = '.' + this.searchName;

        $(document).ready(function(){
            $(document).on("click", "td.click_filter span" , function() {
                $(tableName).bootstrapTable('resetSearch', $(this).html() );
            })
            $(document).on("click", ".mttltable tr i", function(){
                $(this).parents().toggleClass('active');
                console.log($(this).parents() )
            })
        })
    }

    componentDidUpdate() {
        if (!this.props.tableId) {
            $('.' + this.searchName).bootstrapTable('load', this.data);

            const hidden_cols = this.props.hidden_cols
            const curr_hidden = $('.' + this.searchName).bootstrapTable('getHiddenColumns');
            if (hidden_cols) {
                column_types[this.props.columns].forEach((val, index, array) => {
                    const curr  = val['field'];
                    if (hidden_cols.includes(curr) && !curr_hidden.includes(curr)) {
                        $('.' + this.searchName).bootstrapTable('hideColumn', curr);
                    }
                    else if (curr !== 'comments') {
                        $('.' + this.searchName).bootstrapTable('showColumn', curr);
                    }
                });
            }
        }
    }
    componentWillUnmount() {
        $('.' + this.searchName).bootstrapTable('destroy');
    }

    render() {
        this.match = this.props.match;
        if (this.match) {
            this.tableId = this.match.params.tableId;
        }
        else {
            this.tableId = this.props.tableId
        }
        
        if (this.tableId && !this.props.tableId) {
            this.title = this.tableId.toUpperCase();
            this.data = this.props.data[this.title];
            this.metrics = this.props.metrics['work-roles'][this.title];
        } 
        else if (!this.tableId) {
            this.tableId = 'mttl';
            this.title = 'MTTL';
            this.data = this.props.data[this.title];
            this.metrics = this.props.metrics[this.title];
        }
        else {
            this.tableId = this.props.tableId;
            this.title = this.props.title;
            this.data = this.props.data[this.tableId];
            this.metrics = this.props.metrics ? this.props.metrics[this.props.tableId] : null;
        }

        this.searchName = this.tableId + 'table';
        this.columns = column_types[this.props.columns];
        
        const classN = this.searchName + ' table table-sm table-striped table-bordered'
        
        let titleHeader = <h1 style={{ marginBottom: "-50px", marginTop: '25px' }}>{this.title}</h1>;
        let metricsBarTag = <div></div>;
        
        if (this.metrics) {
            titleHeader = <h1>{this.title}</h1>;
            metricsBarTag = <MetricsBar id={this.tableId + "_wte"} data={this.metrics} />;
        }

        return (
            <div className="table_container">
                <button className="clearSearch" onClick={this.clearSearch}><i className="fa fa-times" aria-hidden="true"></i></button>
                {titleHeader}
                {metricsBarTag}
                <table 
                    className={classN}
                >
                </table>
            </div>
        );
    }
}

export default Table;