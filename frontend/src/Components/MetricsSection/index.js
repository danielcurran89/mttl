import React, {Component} from 'react'
import './MetricsSection.scss'

import MetricsBarMTTL from '../MetricsBarMTTL'

class MetricsSection extends Component {
    render() {
        const metricData = this.props.data;
        const te = metricData['count-training-eval'];
        const tc = metricData['count-total']
        const not_cov = tc - (te + (metricData['count-training'] - te) + (metricData['count-eval'] - te));
        return (
            <div className="coverage_container">
                <h3>{this.props.title}</h3>
                <div className="item_metrics_container">
                    <div className="labels_container">
                        <div>
                            <div>
                                <div className="metrics-legend-icon bg-training-eval"></div>
                                <h6>Training and Eval</h6>
                            </div>
                            <p>{te}/{tc}</p>
                        </div>
                        <div>
                            <div>
                                <div className="metrics-legend-icon bg-training"></div>
                                <h6>Training</h6>
                            </div>
                            <p>{metricData['count-training']}/{tc}</p>
                        </div>
                        <div>
                            <div>
                                <div className="metrics-legend-icon bg-eval"></div>
                                <h6>Eval</h6>
                            </div>
                            <p>{metricData['count-eval']}/{tc}</p>
                        </div>
                        <div>
                            <div>
                                <div className="metrics-legend-icon bg-not_cov"></div>
                                <h6>No Training and Eval</h6>
                            </div>
                            <p>{not_cov}/{tc}</p>
                        </div>
                    </div>
                    <div className="metrics_container">
                        <MetricsBarMTTL id={this.props.name + "_wt"} data={metricData} />
                    </div>
                </div>
            </div>
        );
    }
}

export default MetricsSection;