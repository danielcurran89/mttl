import React, {Component} from 'react';
import Table from '../Components/Table'
import './Metrics.scss';

import MetricsSection from '../Components/MetricsSection';
import * as extra_datasets from '../data/extra_datasets.min.json';

class Metrics extends Component {
    getMetricsSections = (data) => {
        let section_list = [];
        for(let [key, val] of Object.entries(data)) {
            section_list.push(<MetricsSection name={key} title={key.replace(/-/g, ' ')} data={val} key={key} />)
        }
        return section_list;
    }

    render() {
        const data = this.props.metrics_data;
        let wr_section_list = this.getMetricsSections(data['work-roles']);
        // let spec_section_list = this.getMetricsSections(data['specializations']);

        return (
            <div>
                <div className="metrics">
                    <h1>Metrics</h1>
                    <MetricsSection name="mttl" title="MTTL Coverage" data={data.MTTL} />
                </div>
                <div className="metrics">
                    <h1>Work Roles</h1>
                    {wr_section_list}
                </div>
                
                <div className="metrics">
                    <h1>Miscellaneous Tables</h1>
                    <Table 
                        name="idf_ksats" 
                        title="IDF KSATs" 
                        data={extra_datasets.default} 
                        rowStyle={true}
                        columns='mttl' 
                        hidden_cols={['proficiency']}
                        tableId='idf_ksats'
                    />
                    <Table 
                        name="wrspec" 
                        title="Work Roles/Specializations" 
                        data={extra_datasets.default} 
                        columns='wrspec' 
                        tableId='wrspec'
                    />
                    <Table 
                        name="nowrspec" 
                        title="KSATs without Work Roles/Specializations" 
                        data={extra_datasets.default} 
                        columns='nowrspec'
                        tableId='nowrspec'
                    />
                </div>
            </div>
        );
    }
}

export default Metrics;