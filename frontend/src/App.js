import React, {Component} from 'react';
import {Route, NavLink, HashRouter as Router, Switch} from "react-router-dom";
import logo from './logo.svg';
import './App.scss';

import Home from "./Home";
import Metrics from "./Metrics";
import Roadmap from "./Roadmap";

import * as metrics_data from './data/metrics.min.json';

const docs = "https://90cos.gitlab.io/mttl/documentation"
const lessons = "https://gitlab.com/90cos/cyt/training/modules"
const kumu = "https://embed.kumu.io/2e6c3a7ed30c0a4e248b51b41382d736"
const po_mqf = "https://gitlab.com/90cos/product-owner-po-eval-study/basic-po/-/blob/master/knowledge/README.md"
const ccd_mqf = "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/blob/master/knowledge/README.md"

const page_url = '';

class App extends Component {
  state = { isTop: true };

  componentDidMount = () => {
    window.addEventListener('scroll', this.handleScroll);
  }
  componentWillUnmount = () => {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = e => {
    let scrollTop = e.srcElement.all[0].scrollTop;
    if (scrollTop > 0) {
      this.setState({isTop: false});
    }
    else {
      this.setState({isTop: true});
    }
  }

  render() {

    return (
      <Router basename={page_url}>
        <div>
        <nav className="navbar sticky-top navbar-expand-lg justify-content-between navbar-light bg-light">
          <a className="navbar-brand" href="/mttl">
            <img src={logo} className="App-logo" alt="logo" width="47" />
            90 COS Master Training Task List
          </a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item active">
                <NavLink className="nav-link" exact to="/" activeClassName='is-active'>Home</NavLink>
              </li>
              <li className="nav-item">
                <a className="nav-link" target="_blank" rel="noopener noreferrer" href={docs}>Docs</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" target="_blank" rel="noopener noreferrer" href={kumu}>Visualization</a>
              </li>
              <li className="nav-item dropdown">
                <button className="nav-link dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Study
                </button>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a className="dropdown-item" target="_blank" rel="noopener noreferrer" href={ccd_mqf}>CCD MQF</a>
                  <a className="dropdown-item" target="_blank" rel="noopener noreferrer" href={po_mqf}>PO MQF</a>
                </div>
              </li>
              <li className="nav-item">
                <a className="nav-link" target="_blank" rel="noopener noreferrer" href={lessons}>Training</a>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" exact to="/roadmap" activeClassName='is-active'>Roadmap</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" exact to="/metrics" activeClassName='is-active'>Metrics</NavLink>
              </li>
            </ul>
          </div>
        </nav>
          <div className="container-fluid">
            <Switch>
              <Route 
                path="/metrics" 
                render={props => <Metrics 
                  {...props}
                  metrics_data={metrics_data.default}
                />}
              />
              <Route path="/Roadmap" component={Roadmap}/>
              <Route 
                path="/" 
                render={props => <Home
                  {...props}
                  page_url={page_url}
                />} 
              />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
