export const YELLOW = '#FFD54F'
export const GREEN = '#81C784'
export const BLUE = '#4FC3F7'

const eval_no_training = {
    'background-color': YELLOW,
    'color': 'black'
}

const training_and_eval = {
    'background-color': GREEN,
    'color': 'black'
}

const training_no_eval = {
    'background-color': BLUE,
    'color': 'black'
}

export function rowStyle(row, index) {
    if (row['training_links'].length > 0 && row['eval_links'].length > 0) {
        return { css: training_and_eval };
    }
    else if (row['training_links'].length > 0) {
        return { css: training_no_eval };
    }
    else if (row['eval_links'].length > 0) {
        return { css: eval_no_training };
    }
    return {};
}

export function setMetrix(id, percent) {
    var i = 0;
    if (i === 0) {
        i = 1;
        var elem = document.getElementById(id);
        var width = 1;
        id = setInterval(frame, 10);
        function frame() {
            if (width >= percent) {
                clearInterval(id);
                i = 0;
            } else {
                width++;
                elem.style.backgroundColor = getGreenToRed(width);
                elem.style.width = width + "%";
                elem.innerHTML = width + "%";
            }           
        }
    }
}

export function getGreenToRed(percent){
    let r = percent<50 ? 255 : Math.floor(255-(percent*2-100)*255/100);
    let g = percent>50 ? 255 : Math.floor((percent*2)*255/100);
    return 'rgb('+r+','+g+',0)';
}

// if (localStorage.getItem('cookieSeen') != 'shown' && localStorage.getItem('consentAgreed') != 'agreed') {
//     $('.init_consent').delay(0).fadeIn();
//     $('body').css('overflow', 'hidden')
//     localStorage.setItem('cookieSeen','shown')
// };
// $('.close').click(function() {
//     $('.init_consent').fadeOut();
//     $('body').css('overflow', 'auto')
//     localStorage.setItem('consentAgreed','agreed')
// })