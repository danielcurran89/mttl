# Searching the MTTL

The search field and display widgets below allow you to search for any phrase in the MTTL listing.  Type your search phrase in the 'Search' field and hit Enter. All KSATs containing the phrase will be listed.

The display widgets allow you to either:
1. View the table in full screen.
2. Filter which columns to see or hide.
3. Download a CSV or Excel version of the MTTL.

![search](../uploads/search.png)