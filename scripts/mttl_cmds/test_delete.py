#!/usr/bin/python3

import os
import argparse
import unittest
import subprocess
import pymongo
import json
import random
from find import find_query
from delete import delete_ksat_data, delete_rel_link_data

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links


class TestMethods(unittest.TestCase):
    def test_delete_ksat(self):
        rand_num = random.randrange(1, 100)
        test_item = list(reqs.find({"training": {"$not": {"$size": 0}}}).skip(rand_num).limit(1))[0]
        # find the item in the db and verify it's correct
        found_item = reqs.find_one({"_id": test_item['_id']})
        self.assertEqual(test_item['_id'], found_item['_id'])
        self.assertEqual(test_item['description'], found_item['description'])
        self.assertEqual(test_item['topic'], found_item['topic'])
        # delete the item
        delete_ksat_data(test_item['_id'])
        # verify the item was deleted
        self.assertEqual(rls.count_documents({"_id": test_item['_id']}), 0)

    def test_delete_rel_link(self):
        rand_num = random.randrange(1, 100)
        test_item = list(rls.find({"KSATs": {"$not": {"$size": 0}}}).skip(rand_num).limit(1))[0]
        found_item = rls.find_one({"_id": test_item['_id']})
        self.assertEqual(test_item['_id'], found_item['_id'])
        # delete the item
        delete_rel_link_data(test_item['_id'])
        rel_id = found_item['_id']
        found_item = rls.find_one({"_id": test_item['_id']})
        # verify item deleted
        self.assertEqual(found_item, None)
        # verify the item is deleted from ksats
        self.assertEqual(reqs.count_documents({"eval": rel_id}), 0)


if __name__ == "__main__":
    unittest.main()