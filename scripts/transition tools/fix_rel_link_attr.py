#!/usr/bin/python3  

import os
import sys
import jsonschema
import json
import pymongo
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import get_all_json
from json_templates import trn_rel_link_item_template, evl_rel_link_item_template
from mttl_cmds.insert import validate_insert_data

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    rls = db.rel_links

    trn = 'training-repos'
    evl = 'eval-repos'

    for rel_link in get_all_json(trn, '.rel-link.json'):
        rel_path_list = rel_link.split('/')
        module_name = rel_path_list[1] # module name
        subject_name = rel_path_list[-1].split('.')[0] # name of the rel-link file

        found = rls.find({'module': subject_name}) # should only return 1 item
        for item in found:
            rls.update_one(
                {'_id': item['_id']}, 
                {
                    '$set': {
                        'module': module_name,
                        'subject': subject_name
                    }
                }
            )

    for rel_link in get_all_json(evl, '.rel-link.json'):
        rel_path_list = rel_link.split('/')
        module_name = rel_path_list[1] # module name
        subject_name = rel_path_list[-1].split('.')[0] # name of the rel-link file

        found = rls.find({'module': subject_name}) # should only return 1 item
        for item in found:
            rls.update_one(
                {'_id': item['_id']}, 
                {
                    '$set': {
                        'module': module_name,
                        'subject': subject_name
                    }
                }
            )        


if __name__ == "__main__":
    main()